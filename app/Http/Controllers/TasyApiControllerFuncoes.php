<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\DAO\TasyDAO;
use App\DAO\DAOUtils;
use App\Classes\Utils;
use PDO;

class TasyApiControllerFuncoes extends Controller
{
    public function __construct()
    {
      $this->conn = DAOUtils::getTasyConnection();
    }


  public function CarregaMediaMedicos (Request $request)
  {
    if($request->header('token') ){
          $stmt = $this->conn->prepare("SELECT  ROUND(SUM(A.DS_DESCRICAO)/COUNT(*),2) MEDIA
                                               ,COUNT(*) QTDE
                                               ,C.CD_MEDICO
                                               ,INITCAP(OBTER_NOME_PF(C.CD_MEDICO)) NOME_MEDICO
                                               --,D.CD_SETOR_ATENDIMENTO
                                               --,E.DS_SETOR_ATENDIMENTO
                                          FROM SAMEL.PES_RESPOSTA A
                                           JOIN ATENDIMENTO_PACIENTE B ON A.NR_ATENDIMENTO = B.NR_ATENDIMENTO
                                           JOIN SAMEL.PES_ATENDIMENTO C ON C.NR_ATENDIMENTO = A.NR_ATENDIMENTO
                                           JOIN ATEND_PACIENTE_UNIDADE D ON D.NR_ATENDIMENTO = B.NR_ATENDIMENTO
                                           JOIN SETOR_ATENDIMENTO E ON E.CD_SETOR_ATENDIMENTO = D.CD_SETOR_ATENDIMENTO
                                         WHERE A.CD_PERGUNTA = 'A1'
                                           AND A.DS_DESCRICAO IS NOT NULL
                                           AND C.CD_MEDICO = :CD_MEDICO
                                          GROUP BY C.CD_MEDICO
                                                  --,D.CD_SETOR_ATENDIMENTO
                                                  --,E.DS_SETOR_ATENDIMENTO");

          $stmt->bindValue(':CD_MEDICO', $request['CD_MEDICO']);
          $stmt->execute();
          $data =  $stmt->fetchAll(PDO::FETCH_ASSOC);

          return response()->json($data);
      }else {
        return response()->json(['err'=>'token not found'],404);
      }
  }

  public function authCRM (Request $request)
  {
    if($request->header('token') ){
          $stmt = $this->conn->prepare("SELECT  a.cd_pessoa_fisica
                                          	   ,SUBSTR(obter_nome_pf(a.cd_pessoa_fisica),1,255) nm_pessoa_fisica
                                          	   ,b.nr_crm
                                          	   ,b.nm_guerra
                                          	   ,substr(obter_especialidade_medico(a.cd_pessoa_fisica,'D'),1,40) ds_especialidade
                                               ,c.nm_usuario
                                          FROM  pessoa_fisica a,medico b,usuario c
                                          WHERE  a.cd_pessoa_fisica = b.cd_pessoa_fisica
                                            AND c.cd_pessoa_fisica = a.cd_pessoa_fisica
                                          	AND	b.ie_situacao = 'A'
                                            AND b.nr_crm = :NR_CRM
                                          ORDER BY a.nm_pessoa_fisica");

          $stmt->bindValue(':NR_CRM', $request['NR_CRM']);
          $stmt->execute();
          $data =  $stmt->fetchAll(PDO::FETCH_ASSOC);

          return response()->json($data);
      }else {
        return response()->json(['err'=>'token not found'],404);
      }
  }



}
