<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\DAO\TasyDAO;
use App\DAO\DAOUtils;
use App\Classes\Utils;
use \Firebase\JWT\JWT;
use PDO;

class AuthController extends Controller
{
	public function __construct()
	{
		$this->conn = DAOUtils::getTasyConnection();
	}

	public function authentic(Request $req){

		if($req->input('cd')){
			$stmt = $this->conn->prepare("SELECT  a.cd_pessoa_fisica
																					 ,SUBSTR(obter_nome_pf(a.cd_pessoa_fisica),1,255) nm_pessoa_fisica
																					 ,b.nr_crm
																					 ,b.nm_guerra
																					 ,substr(obter_especialidade_medico(a.cd_pessoa_fisica,'D'),1,40) ds_especialidade
																					 ,c.nm_usuario
																			FROM  pessoa_fisica a,medico b,usuario c
																			WHERE  a.cd_pessoa_fisica = b.cd_pessoa_fisica
																				AND c.cd_pessoa_fisica = a.cd_pessoa_fisica
																				AND	b.ie_situacao = 'A'
																				AND b.nr_crm = :NR_CRM
																			ORDER BY a.nm_pessoa_fisica");
			$stmt->bindValue(':NR_CRM', $req->input('cd'));
			$exec = $stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if(isset($data[0]['CD_PESSOA_FISICA']))
				return response()->json(['token'=>$this->jwt($req->input('cd'))],200);
			else
				return response()->json(['err'=>'Unauthorized person'],401);
		}else
		return response()->json(['err'=>'parameter not valid'],500);
	}

	public function validToken($token){
		//$payload=$this->decode_jwt($token);
		return true;
	}
	protected function jwt($id){
		$payload=[
			'iss'=>'lumen-jwt',
			'sub'=>$id,
			'iat'=>time(),
			'exp'=>time()+60*60
		];
		return JWT::encode($payload,env('JWT_SECRET'));
	}

	protected function decode_jwt($token){
		return JWT::decode($token,env('JWT_SECRET'));
	}


	public function test(){
		return phpinfo();
	}


}
