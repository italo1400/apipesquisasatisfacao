<?php

namespace App\DAO;

use PDO;

class TasyDAO
{
    public function __construct()
    {
        $this->conn = DAOUtils::getTasyConnection();
    }

    public function getDadosAgendamento($args)
    {
        $stmt = null;
        if (isset($args['nr_seq_agenda'])) {
            $stmt = $this->conn->prepare('select
                                              A.*,OBTER_EMAIL_PF(A.CD_PESSOA_FISICA_PACIENTE) DS_EMAIL
                                    from
                                        samel.vw_dados_agendamento A
                                    where
                                        nr_seq_agenda = :nr_seq_agenda');
            $stmt->bindValue(':nr_seq_agenda', $args['nr_seq_agenda']);
        } elseif (isset($args['cd_pessoa_fisica'])) {
            $stmt = $this->conn->prepare('select
                                              A.*,OBTER_EMAIL_PF(A.CD_PESSOA_FISICA_PACIENTE) DS_EMAIL
                                    from
                                        samel.vw_dados_agendamento A
                                    where
                                        cd_pessoa_fisica_paciente = :cd_pessoa_fisica');
            $stmt->bindValue(':cd_pessoa_fisica', $args['cd_pessoa_fisica']);
        } elseif (isset($args['nr_cpf'])) {
            $stmt = $this->conn->prepare('select
                                              A.*,OBTER_EMAIL_PF(A.CD_PESSOA_FISICA_PACIENTE) DS_EMAIL
                                    from
                                        samel.vw_dados_agendamento A
                                    where
                                        a.cd_pessoa_fisica_paciente in (select
                                                                            cd_pessoa_fisica
                                                                        from
                                                                            pessoa_fisica
                                                                        where
                                                                            nr_cpf = :nr_cpf)
                                        and trunc(a.dt_agenda) = trunc(sysdate)');
            $stmt->bindValue(':nr_cpf', $args['nr_cpf']);
        } elseif (isset($args['nm_paciente']) && isset($args['dt_nascimento'])) {
            $stmt = $this->conn->prepare("select
                                              A.*,OBTER_EMAIL_PF(A.CD_PESSOA_FISICA_PACIENTE) DS_EMAIL
                                    from
                                        samel.vw_dados_agendamento A
                                    where
                                        a.cd_pessoa_fisica_paciente in (select
                                                                            cd_pessoa_fisica
                                                                        from
                                                                            pessoa_fisica
                                                                        where
                                                                            lower(nm_paciente) = lower(:nm_paciente)
                                                                            and trunc(dt_nascimento) = to_date(:dt_nascimento,'yyyy-mm-dd'))
                                        and trunc(a.dt_agenda) = trunc(sysdate)");
            $stmt->bindValue(':nm_paciente', $args['nm_paciente']);
            $d = new \DateTime($args['dt_nascimento']);
            $stmt->bindValue(':dt_nascimento', $d->format('Y-m-d'));
        }
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($data) > 0) {
            return $data;
        } else {
            return null;
        }
    }

    public function getEspecialidadesDoDia()
    {
        $stmt = $this->conn->prepare("SELECT DISTINCT a.cd_especialidade cd,
                                                  obter_desc_espec_medica(a.cd_especialidade) ds
                                  FROM AGENDA A
                                  WHERE A.cd_agenda IN (
                                          SELECT A.cd_agenda
                                          FROM    AGENDA_CONSULTA a
                                          WHERE 1 = 1
                                              AND ie_status_agenda <> 'II'
                                              AND ie_status_agenda <> 'C'
                                              AND substr(a.CD_AGENDA_EXTERNA,1,1) in ('2','3')
                                              AND trunc(dt_agenda) = trunc(sysdate))
                                  order by 2");
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    public function getMedicosDaEspecialidade($cdEspecialidade)
    {
        $stmt = $this->conn->prepare("SELECT  distinct
                                        obter_nome_pf(b.cd_pessoa_fisica) nome,
                                        obter_desc_espec_medica(b.cd_especialidade) especialidade,
                                        b.CD_AGENDA_EXTERNA cons_sala,
                                        a.cd_agenda,
                                        CASE substr(b.CD_AGENDA_EXTERNA,1,1) WHEN '2' THEN 40
                                                                             WHEN '3' THEN 29
                                        END FILA_NORMAL,
                                        CASE substr(b.CD_AGENDA_EXTERNA,1,1) WHEN '2' THEN 41
                                                                             WHEN '3' THEN 30
                                        END FILA_PREFERENCIAL,
                                        substr(b.CD_AGENDA_EXTERNA,1,1) ANDAR
                                FROM    AGENDA_CONSULTA a, AGENDA b
                                WHERE 1 = 1
                                    AND a.cd_agenda = b.cd_agenda
                                    AND a.ie_status_agenda <> 'II'
                                    AND a.ie_status_agenda <> 'C'
                                    AND substr(b.CD_AGENDA_EXTERNA,1,1)  in ('2','3')
                                    AND b.cd_especialidade = :id
                                    AND trunc(a.dt_agenda) = trunc(sysdate)
                                ORDER BY 1");
        $stmt->bindValue(':id', $cdEspecialidade);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    public function getDadosFila($idFila)
    {
        $stmt = $this->conn->prepare('select DS_LETRA_VERIFICACAO,DS_FILA,DS_CURTO from fila_espera_senha where nr_sequencia = :nr_sequencia');
        $stmt->bindValue(':nr_sequencia', $idFila);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data;
    }

    public function gerarSenhaFila($args, &$nrSenha, &$nrSeqSenha)
    {
        $idFila = $args['idFila'];
        $nmUsuario = $args['nmUsuario'];
        $stmt = $this->conn->prepare("CALL gerar_senha_paciente(nr_seq_fila_p=>$idFila, cd_estabelecimento_p=>1, nm_usuario_p=>'$nmUsuario', nr_senha_p=>:nr_senha_p, nr_seq_senha_p=>:nr_seq_senha_p)");
        $stmt->bindParam(':nr_senha_p', $nrSenha, PDO::PARAM_STR, 15);
        $stmt->bindParam(':nr_seq_senha_p', $nrSeqSenha, PDO::PARAM_STR, 15);
        $stmt->execute();
    }

    public function getDadosSenha($nrSeqSenha)
    {
        $stmt = $this->conn->prepare("select
                                        *
                                      from
                                        samel.vw_dados_senha a
                                      where
                                        a.nr_seq_senha = :nr_seq_senha_p");
        $stmt->bindValue(':nr_seq_senha_p', $nrSeqSenha);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data;
    }

    public function gerarAtendimentoAgendaSamel($dadosAgenda)
    {
        $dsMsgErro = null;
        $nrAtendimento = null;
        $nrSeqSenha = null;
        $stmt = $this->conn->prepare("CALL samel.gerar_atendimento_ag_samel(nr_seq_agenda_p1 => :nr_seq_agenda, ie_prioridade => :ie_prioridade, ds_msg_erro_o => :ds_msg_erro, nr_atendimento_o => :nr_atendimento, nr_seq_senha_o => :nr_seq_senha)");
        $stmt->bindParam(':nr_seq_agenda', $dadosAgenda['NR_SEQ_AGENDA']);
        $stmt->bindValue(':ie_prioridade', $dadosAgenda['IE_PRIORIDADE']);
        $stmt->bindParam(':ds_msg_erro', $dsMsgErro, PDO::PARAM_STR, 15);
        $stmt->bindParam(':nr_atendimento', $nrAtendimento, PDO::PARAM_STR, 15);
        $stmt->bindParam(':nr_seq_senha', $nrSeqSenha, PDO::PARAM_STR, 15);
        $resultExec = $stmt->execute();
        $result = array();
        if (!$resultExec) {
            $result['status'] = false;
            $result['ds_msg_erro'] = implode(' - ',$this->conn->errorInfo());
        } else {
            if (!isset($dsMsgErro)) {
                $result['status'] = false;
                $result['ds_msg_erro'] = $dsMsgErro;
            } else {
                $result['status'] = true;
                $result['nr_seq_senha'] = $nrSeqSenha;
                $result['nr_atendimento'] = $nrAtendimento;
            }
        }
        return $result;
    }

    public function vincularSenhaAgenda($nrSeqSenha, $nrSeqAgenda)
    {
      $stmt = $this->conn->prepare("CALL tasy.vincular_senha_agendamento(:nr_seq_senha_p, :nr_seq_agenda_p, IE_TIPO_AGENDA_P=>'S')");
      $stmt->bindParam(':nr_seq_senha_p',$nrSeqSenha);
      $stmt->bindParam(':nr_seq_agenda_p',$nrSeqAgenda);
      return $stmt->execute();
    }

    public function getDadosAtendimento($nrAtendimento)
    {
        $stmt = $this->conn->prepare('select * from atendimento_paciente_v where nr_atendimento = :nr_atendimento');
        $stmt->bindParam(':nr_atendimento', $nrAtendimento);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data;
    }

    public function updateNomePacienteSenha($nrSeqSenha, $nmPaciente){
        $stmt = $this->conn->prepare("update paciente_senha_fila set nm_paciente = :nm_paciente where nr_sequencia = :nr_seq_senha");
        $stmt->bindParam(':nm_paciente', $nmPaciente);
        $stmt->bindParam(':nr_seq_senha', $nrSeqSenha);
        return $stmt->execute();
    }

    public function execSql($sql)
    {
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data;
    }
}
